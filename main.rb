require 'rubygems'
require 'sinatra'
require 'haml'
require 'tinspitz'

get '/' do
  haml :index
end
